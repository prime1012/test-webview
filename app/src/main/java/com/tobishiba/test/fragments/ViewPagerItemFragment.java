package com.tobishiba.test.fragments;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.tobishiba.test.R;
import com.tobishiba.test.models.Meme;

public class ViewPagerItemFragment extends Fragment {
    private static final String BUNDLE_KEY_IMAGE_RESOURCE_ID    = "bundle_key_image_resource_id";
    private int                 mImageResourceId;

    public static ViewPagerItemFragment instantiateWithArgs(final Context context, final Meme meme) {
        final ViewPagerItemFragment fragment = (ViewPagerItemFragment) instantiate(context, ViewPagerItemFragment.class.getName());
        final Bundle args = new Bundle();
        args.putInt(BUNDLE_KEY_IMAGE_RESOURCE_ID, meme.mImageResourceId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initArguments();
    }

    private void initArguments() {
        final Bundle arguments = getArguments();
        if(arguments != null) {
            mImageResourceId = arguments.getInt(BUNDLE_KEY_IMAGE_RESOURCE_ID);
        }
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container, final Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.meme_view_pager_item_fragment, container, false);
        initViews(view);
        return view;
    }

    private void initViews(final View view) {
        initImage(view);
    }

    private void initImage(final View view) {
        final ImageView backgroundImage = view.findViewById(R.id.meme_view_pager_item_fragment_image);
        backgroundImage.setImageResource(mImageResourceId);
    }
}
