package com.tobishiba.test.activities;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import com.tobishiba.test.R;
import com.tobishiba.test.adapters.ViewPagerAdapter;
import com.tobishiba.test.library.CircularViewPagerHandler;
import com.tobishiba.test.models.Meme;

public class MainActivity extends FragmentActivity {

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initViews();
    }

    private void initViews() {
        final ViewPager viewPager = findViewById(R.id.activity_main_view_pager);
        viewPager.setAdapter(new ViewPagerAdapter(this, getFragmentManager(), Meme.createSampleMemes()));
        final CircularViewPagerHandler circularViewPagerHandler = new CircularViewPagerHandler(viewPager);
        circularViewPagerHandler.setOnPageChangeListener(createOnPageChangeListener());
        viewPager.addOnPageChangeListener(circularViewPagerHandler);
    }

    private ViewPager.OnPageChangeListener createOnPageChangeListener() {

        return new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(final int position, final float positionOffset, final int positionOffsetPixels) {}
            @Override
            public void onPageSelected(final int position) {}
            @Override
            public void onPageScrollStateChanged(final int state) {}
        };
    }
}
