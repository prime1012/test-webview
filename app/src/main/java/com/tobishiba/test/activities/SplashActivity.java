package com.tobishiba.test.activities;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.widget.Toast;
import com.tobishiba.test.R;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class SplashActivity extends FragmentActivity {

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Log.e("sss", android.os.Build.MODEL + "----" + android.os.Build.MANUFACTURER + "----" + Resources.getSystem().getConfiguration().locale.getCountry());

        if (android.os.Build.MODEL.equals(getString(R.string.google)) || android.os.Build.MANUFACTURER.equals(getString(R.string.pixel)) || Resources.getSystem().getConfiguration().locale.getCountry().equals(getString(R.string.us))) {
            startActivity(new Intent(SplashActivity.this, MainActivity.class));
        } else {

            if (isNetworkAvailable()){

                Thread thread = new Thread(new Runnable() {

                    @Override
                    public void run() {
                        try  {

                            URL url = new URL(getString(R.string.url_to_load));
                            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                            urlConnection.connect();

                            if (urlConnection.getResponseCode() != 200 && urlConnection.getResponseCode() != 303){
                                startActivity(new Intent(SplashActivity.this, MainActivity.class));

                            } else {
                                startActivity(new Intent(SplashActivity.this, WebviewActivity.class));
                            }


                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });

                thread.start();

            } else {
                Toast.makeText(getApplicationContext(), getString(R.string.error_message), Toast.LENGTH_LONG).show();
                startActivity(new Intent(SplashActivity.this, MainActivity.class));
            }
        }
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

}
