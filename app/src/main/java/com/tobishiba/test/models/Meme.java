package com.tobishiba.test.models;

import com.tobishiba.test.R;
import java.util.ArrayList;
import java.util.List;

public class Meme {

    public int      mImageResourceId;

    public static List<Meme> createSampleMemes() {
        final List<Meme> memes = new ArrayList<Meme>();
        memes.add(new Meme(R.drawable.meme_wondering));
        memes.add(new Meme( R.drawable.meme_determined));
        memes.add(new Meme(R.drawable.meme_crying));
        return memes;
    }

    private Meme(final int imageResourceId) {
        mImageResourceId = imageResourceId;
    }
}
