package com.tobishiba.test.adapters;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import com.tobishiba.test.fragments.ViewPagerItemFragment;
import com.tobishiba.test.library.BaseCircularViewPagerAdapter;
import com.tobishiba.test.models.Meme;
import java.util.List;

public class ViewPagerAdapter extends BaseCircularViewPagerAdapter<Meme> {
    private final Context mContext;

    public ViewPagerAdapter(final Context context, final FragmentManager fragmentManager, final List<Meme> memes) {
        super(fragmentManager, memes);
        mContext = context;
    }

    @Override
    protected Fragment getFragmentForItem(final Meme meme) {
        return ViewPagerItemFragment.instantiateWithArgs(mContext, meme);
    }
}
